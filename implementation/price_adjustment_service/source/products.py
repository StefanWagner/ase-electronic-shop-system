class Products:
  def __init__(self) -> None:
    self.products: [] = []

  def get_products(self) -> []:
    return self.products
