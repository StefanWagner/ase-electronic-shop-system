from json import JSONEncoder


class Encoder(JSONEncoder):
  def default(self, object):
    return object.__dict__
