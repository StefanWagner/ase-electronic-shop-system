from products import Products


class Recommendation:
  def __init__(self, recommendation: float, products: Products) -> None:
    self.recommendation: float = recommendation
    self.products: Products = products
