from bs4 import BeautifulSoup
from flask import Flask, request, abort, Response
import requests
import json
from product import Product
from products import Products
from encoder import Encoder
from recommendation import Recommendation
import logging

app = Flask(__name__)

logging.basicConfig(format='[%(asctime)s]: %(message)s', level=logging.INFO, datefmt='%H:%M:%S')

@app.route('/recommendations', methods=['GET'])
def get_recommendation() -> Response:
  """
  This method provides access to the price recommendations.
  :return: The found recommendations as json.
  """
  if request.method == 'GET':
    if request.args.get('search'):
      logging.info('Recommendations request with search: %s', request.args.get('search', type=str))
      return get_geizhals_recommendation(request.args.get('search', type=str))
    return Response('No or wrong get parameter!', status=400, mimetype='text/plain')
  return Response('Unsupported http method!', status=400, mimetype='text/plain')


def get_geizhals_recommendation(search: str) -> Response:
  """
  This method extracts important price recommendation information from the geizhals.at web page.
  :return: The found recommendations as json.
  """
  url: str = 'https://geizhals.at/?fs=' + search + '&hloc=at&in='
  headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'
  }
  page = requests.get(url, headers)
  htmlPage = BeautifulSoup(page.content, 'html.parser')
  geizhals_products = htmlPage.findAll(True, {'class': 'listview__content'})
  if geizhals_products:
    logging.info('Recommendations found at geizhals.at.')
    products: Products = Products()
    for geizhals_product in geizhals_products:
      name: str = geizhals_product.find(True, {'class': 'listview__name-link'}).get_text().replace('\n', '')
      price_str: str = geizhals_product.find(True, {'class': 'price'})
      if price_str is None:
        continue
      price: float = float(price_str.get_text().replace(' ', '').replace('€', '').replace(',', '.'))
      product: Product = Product(name, price)
      products.get_products().append(product)
    recommendation: Recommendation = Recommendation(products.get_products()[0].get_price(), products)
    recommendation_json = json.dumps(recommendation, indent=4, cls=Encoder)
    return Response(recommendation_json, status=200, mimetype='application/json')
  return Response('No recommendation found for the given search term!', status=404, mimetype='text/plain')


@app.route('/health', methods=['GET'])
def health() -> Response:
  return Response('Price adjustment service is running!', status=200, mimetype='text/plain')


if __name__ == '__main__':
  app.run(host='0.0.0.0', port=3803, debug=True)
