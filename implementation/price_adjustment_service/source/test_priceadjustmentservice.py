import unittest
from priceadjustmentservice import get_geizhals_recommendation
from flask import Flask

app2 = Flask(__name__)

class TestGetGeizhalsRecommendation(unittest.TestCase):
  def test_get_geizhals_recommendation(self):
    response = get_geizhals_recommendation('samsung')
    self.assertEqual(200, response._status_code)