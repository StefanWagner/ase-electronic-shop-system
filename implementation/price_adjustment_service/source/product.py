class Product:
  def __init__(self, name: str, price: float) -> None:
    self.name: str = name
    self.price: float = price

  def get_price(self) -> float:
    return self.price
