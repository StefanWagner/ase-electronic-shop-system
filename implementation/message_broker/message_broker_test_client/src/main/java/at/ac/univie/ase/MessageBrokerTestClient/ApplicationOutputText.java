package at.ac.univie.ase.MessageBrokerTestClient;

import io.github.lukas_krickl.cli_client.visual.OutputText;
import io.github.lukas_krickl.cli_client.visual.TextColor;

public class ApplicationOutputText implements OutputText {
    private final static String DEFAULT_INPUT_PREFIX = "MessageBrokerClient > ";
    private static String inputPrefix = DEFAULT_INPUT_PREFIX;


    @Override
    public String getApplicationLogo() {
        return "___  ___                                ______           _             \n" +
                "|  \\/  |                                | ___ \\         | |            \n" +
                "| .  . | ___  ___ ___  __ _  __ _  ___  | |_/ /_ __ ___ | | _____ _ __ \n" +
                "| |\\/| |/ _ \\/ __/ __|/ _` |/ _` |/ _ \\ | ___ \\ '__/ _ \\| |/ / _ \\ '__|\n" +
                "| |  | |  __/\\__ \\__ \\ (_| | (_| |  __/ | |_/ / | | (_) |   <  __/ |   \n" +
                "\\_|  |_/\\___||___/___/\\__,_|\\__, |\\___| \\____/|_|  \\___/|_|\\_\\___|_|   \n" +
                "                             __/ |                                     \n" +
                "                            |___/                                      \n" +
                " _____         _     _____ _ _            _   \n" +
                "|_   _|       | |   /  __ \\ (_)          | |  \n" +
                "  | | ___  ___| |_  | /  \\/ |_  ___ _ __ | |_ \n" +
                "  | |/ _ \\/ __| __| | |   | | |/ _ \\ '_ \\| __|\n" +
                "  | |  __/\\__ \\ |_  | \\__/\\ | |  __/ | | | |_ \n" +
                "  \\_/\\___||___/\\__|  \\____/_|_|\\___|_| |_|\\__|\n" +
                "                                              ";
    }

    @Override
    public String getLicence() {
        return "";
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public String initialisationText() {
        return "\n\nDefault connection configuration: localhost:9092 \n\nUse 'topic' command to configure a topic first";
    }

    @Override
    public String getInputPrefix() {
        return inputPrefix;
    }

    public static void setInputPrefix(Topic topic) {
        if (topic == Topic.none) {
            inputPrefix = DEFAULT_INPUT_PREFIX;
        } else {
            inputPrefix = DEFAULT_INPUT_PREFIX+TextColor.BRIGHT_GREEN.apply(topic.name())+" > ";
        }
    }
}
