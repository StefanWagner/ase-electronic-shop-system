package at.ac.univie.ase.MessageBrokerTestClient.commands;

import at.ac.univie.ase.MessageBrokerTestClient.MessageBrokerSender;
import at.ac.univie.ase.MessageBrokerTestClient.Topic;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.commands.CommandInfo;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

public class SendMessageCommand extends Command {
    private final MessageBrokerSender messageBrokerSender = MessageBrokerSender.getInstance();
    public SendMessageCommand() {
        super(new CommandInfoBuilder("send messages")
                .addSyntax("send")
                .setRequiresArgs(ArgsRequired.REQUIRED)
                .setDescription("Sends the given message as is to the configured topic.")
                .build());
    }

    @Override
    public void run() {
        if (messageBrokerSender.getTopic() == Topic.none) {
            UserInterface.printSection(TextColor.RED.apply("No topic configured. Use the 'topic command' first"));
        } else {
            messageBrokerSender.sendMessage(getArgs());
        }
    }
}
