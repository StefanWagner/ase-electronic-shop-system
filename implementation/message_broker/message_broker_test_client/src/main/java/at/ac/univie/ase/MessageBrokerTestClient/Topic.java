package at.ac.univie.ase.MessageBrokerTestClient;

public enum Topic {
    users,
    orders,
    products,
    notification,
    none
}
