package at.ac.univie.ase.MessageBrokerTestClient;

import org.apache.kafka.clients.KafkaClient;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class MessageBrokerSender {
    private String brokerHost = "message-broker";
    private String brokerPort = "9092";
    private Topic topic = Topic.none;

    private KafkaProducer<Long, String> client;
    private static final MessageBrokerSender instance = new MessageBrokerSender();

    private MessageBrokerSender() {
        this.client = createBrokerClient();
    }

    public static MessageBrokerSender getInstance() {
        return instance;
    }

    public void sendMessage(String message){
        ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(topic.name(),1L,  message);
        client.send(record);
    }

    private KafkaProducer<Long, String> createBrokerClient(){
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerHost+":"+brokerPort);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "MessageBrokerTestClient");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        //props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, CustomPartitioner.class.getName());
        return new KafkaProducer<>(props);
    }

    public Topic getTopic() {
        return topic;
    }

    public String getBrokerHost() {
        return brokerHost;
    }

    public String getBrokerPort() {
        return brokerPort;
    }

    public void setBrokerHost(String brokerHost) {
        this.brokerHost = brokerHost;
        this.client.close();
        this.client = createBrokerClient();
    }

    public void setBrokerPort(String brokerPort) {
        this.brokerPort = brokerPort;
        this.client.close();
        this.client = createBrokerClient();
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
        ApplicationOutputText.setInputPrefix(topic);
    }
}
