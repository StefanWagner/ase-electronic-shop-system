package at.ac.univie.ase.MessageBrokerTestClient.commands;

import at.ac.univie.ase.MessageBrokerTestClient.MessageBrokerSender;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.commands.CommandInfo;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

public class ConfigureCommand extends Command {
    private final MessageBrokerSender messageBrokerSender = MessageBrokerSender.getInstance();

    public ConfigureCommand() {
        super(new CommandInfoBuilder("configure connection")
                .addSyntax("config")
                .addSyntax("configure")
                .setDescription("Displays the current connection configuration. " +
                        "Changes the configuration, when a parameters are given.")
                .setRequiresArgs(ArgsRequired.OPTIONAL)
                .addParameterList()
                .addParameter("-host", "Changes the host. Default is 'message-broker'")
                .addParameter("-port", "Changes the port. Default is '9092'")
                .finishParameterList()
                .build());
    }

    @Override
    public void run() {
        switch(getParams().size()) {
            case 0: {
                UserInterface.println(messageBrokerSender.getBrokerHost()+":"+messageBrokerSender.getBrokerPort());
                break;
            }
            case 1: {
                if (hasParameter("-host")) {
                    messageBrokerSender.setBrokerHost(getArgs());

                } else if (hasParameter("-port")){
                    messageBrokerSender.setBrokerPort(getArgs());
                }
                break;
            }
            case 2: {
                UserInterface.println(TextColor.RED.apply("You can only use one parameter at a time."));
                break;
            }
        }
    }
}
