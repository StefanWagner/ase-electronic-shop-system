package at.ac.univie.ase.MessageBrokerTestClient;

import at.ac.univie.ase.MessageBrokerTestClient.commands.ChangeTopicCommand;
import at.ac.univie.ase.MessageBrokerTestClient.commands.ConfigureCommand;
import at.ac.univie.ase.MessageBrokerTestClient.commands.SendMessageCommand;
import io.github.lukas_krickl.cli_client.builder.application.ApplicationManagerBuilder;
import io.github.lukas_krickl.cli_client.logic.ApplicationManager;

import javax.naming.NamingException;

public class MessageBrokerTestClientApplication {
    public static void main(String[] args) throws NamingException {
        ApplicationManager appMan = new ApplicationManagerBuilder()
                .setupOutputText(new ApplicationOutputText())
                .addCommand(new ChangeTopicCommand())
                .addCommand(new ConfigureCommand())
                .addCommand(new SendMessageCommand())
                .build();
        appMan.start();
    }
}
