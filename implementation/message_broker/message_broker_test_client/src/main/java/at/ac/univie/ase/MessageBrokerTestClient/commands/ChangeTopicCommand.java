package at.ac.univie.ase.MessageBrokerTestClient.commands;

import at.ac.univie.ase.MessageBrokerTestClient.MessageBrokerSender;
import at.ac.univie.ase.MessageBrokerTestClient.Topic;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

public class ChangeTopicCommand extends Command {
    private final MessageBrokerSender messageBrokerSender = MessageBrokerSender.getInstance();

    public ChangeTopicCommand() {
        super(new CommandInfoBuilder("change topic")
                .addSyntax("topic")
                .setDescription("Displays the current topic. Changes the topic if a new topic name is provided. e.g. topic users")
                .setRequiresArgs(ArgsRequired.OPTIONAL)
                .build());
    }


    @Override
    public void run() {
        String givenTopic = getArgs();
        if (givenTopic.isEmpty()) {
            UserInterface.println(messageBrokerSender.getTopic().name());

        } else {
            try {
                Topic selected = Topic.valueOf(givenTopic);
                messageBrokerSender.setTopic(selected);
            } catch (IllegalArgumentException e) {
                UserInterface.println(TextColor.RED.apply("Given topic: '" + givenTopic + "' does not match any configured topics!"));
            }

        }
    }
}
