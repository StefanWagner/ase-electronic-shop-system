package at.ac.univie.ase3.apigateway.api.v1;

public enum Roles {
    CUSTOMER,
    VENDOR,
    ADMIN
}
