package at.ac.univie.ase3.apigateway.api.v1.routes;

import at.ac.univie.ase3.apigateway.api.v1.AuthorizationGatewayFilterFactory;
import at.ac.univie.ase3.apigateway.api.v1.Roles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

import javax.annotation.PostConstruct;

@Service
public class UserServiceRoutes {
    @Value("${services.uri.user}")
    private String userServiceUri;

    @Bean
    public RouteLocator userRoutes(RouteLocatorBuilder builder
            , AuthorizationGatewayFilterFactory authFilterFactory) {

        return builder.routes()
                .route("createUser", p -> p
                        .path("/v1/user/createUser")
                        .and().method(HttpMethod.POST)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(userServiceUri))

                .route("createVendor", p -> p
                        .path("/v1/user/createVendor")
                        .and().method(HttpMethod.POST)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(userServiceUri))

                .route("createAdmin", p -> p
                        .path("/v1/user/createAdmin")
                        .and().method(HttpMethod.POST)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(userServiceUri))

                .route("getAddresses", p -> p
                        .path("/v1/user/address/{userID}")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(userServiceUri))

                .route("getAddress", p -> p
                        .path("/v1/user/address/{userID}/{street}")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(userServiceUri))

                .route("editAddress", p -> p
                        .path("/v1/user/editAddress")
                        .and().method(HttpMethod.PATCH)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(userServiceUri))

                .route("createAddress", p -> p
                        .path("/v1/user/createAddress")
                        .and().method(HttpMethod.POST)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(userServiceUri))

                .route("getProfile", p -> p
                        .path("/v1/user/users/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(userServiceUri))

                .route("editProfile", p -> p
                        .path("/v1/user/editProfile")
                        .and().method(HttpMethod.PATCH)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(userServiceUri))

                .route("health", p -> p
                        .path("/v1/user/health")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(userServiceUri))

                .build();
    }
}
