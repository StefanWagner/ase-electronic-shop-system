package at.ac.univie.ase3.apigateway.api.v1.routes;


import at.ac.univie.ase3.apigateway.api.v1.AuthorizationGatewayFilterFactory;
import at.ac.univie.ase3.apigateway.api.v1.Roles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceRoutes {
    @Value("${services.uri.auth}")
    private String authServiceUri;

    @Bean
    public RouteLocator authentication(RouteLocatorBuilder builder,
                                       AuthorizationGatewayFilterFactory authFilterFactory) {
        return builder.routes()
                .route("authentication_route", p -> p
                        .path("/v1/auth/authenticate")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(authServiceUri))
                .route("invalidate_route", p -> p
                        .path("/v1/auth/invalidate/{userID}")
                        .and().method(HttpMethod.DELETE)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(authServiceUri))
                .route("change_password_route", p -> p
                        .path("/v1/auth/users/{userID}/password")
                        .and().method(HttpMethod.PUT)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(authServiceUri))
                .route("healthcheck", p -> p
                        .path("/v1/auth/actuator/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .filter(authFilterFactory.apply(
                                        new AuthorizationGatewayFilterFactory
                                                .Config(Roles.ADMIN)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(authServiceUri))
                .build();
    }


}
