package at.ac.univie.ase3.apigateway.api.v1.routes;

import at.ac.univie.ase3.apigateway.api.v1.AuthorizationGatewayFilterFactory;
import at.ac.univie.ase3.apigateway.api.v1.Roles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

@Service
public class ShippingServiceRoutes {
    @Value("${services.uri.shipp}")
    private String shippingServiceUri;

    @Bean
    public RouteLocator shippingRoutes(RouteLocatorBuilder builder
            , AuthorizationGatewayFilterFactory authFilterFactory) {

        return builder.routes()
                .route("shipping_orders", p -> p
                        .path("/v1/shipp/orders/users/{userID}/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .filter(authFilterFactory.apply(
                                        new AuthorizationGatewayFilterFactory
                                                .Config(Roles.CUSTOMER)))
                                .stripPrefix(2))
                        .uri(shippingServiceUri))
                .route("healthcheck", p -> p
                        .path("/v1/shipp/actuator/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .filter(authFilterFactory.apply(
                                        new AuthorizationGatewayFilterFactory
                                                .Config(Roles.ADMIN)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(shippingServiceUri))
                .build();
    }
}
