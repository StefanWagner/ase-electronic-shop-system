package at.ac.univie.ase3.apigateway.api.v1.routes;

import at.ac.univie.ase3.apigateway.api.v1.AuthorizationGatewayFilterFactory;
import at.ac.univie.ase3.apigateway.api.v1.Roles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;
import org.springframework.http.HttpMethod;
import javax.annotation.PostConstruct;

@Service
public class NotificationServiceRoutes {
    @Value("${services.uri.notif}")
    private String notificationServiceUri;

    @Bean
    public RouteLocator notificationRoutes(RouteLocatorBuilder builder
            , AuthorizationGatewayFilterFactory authFilterFactory) {

        return builder.routes()
                .route("healthcheck", p -> p
                        .path("/v1/notif/healthcheck")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                // .filter(authFilterFactory.apply(
                                //         new AuthorizationGatewayFilterFactory
                                //                 .Config(Roles.ADMINISTRATOR))
                                // )       
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(notificationServiceUri))
                .route("sendNotification", p -> p
                        .path("/v1/notif/sendNotification")
                        .and().method(HttpMethod.POST)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                // .filter(authFilterFactory.apply(
                                //         new AuthorizationGatewayFilterFactory
                                //                 .Config(Roles.ADMINISTRATOR))
                                // )       
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(notificationServiceUri)) 
                .route("testBroker", p -> p
                .path("/v1/notif/testBroker")
                .and().method(HttpMethod.POST)
                .filters(gatewayFilterSpec -> gatewayFilterSpec
                        // .filter(authFilterFactory.apply(
                        //         new AuthorizationGatewayFilterFactory
                        //                 .Config(Roles.ADMINISTRATOR))
                        // )       
                        .stripPrefix(2)
                        .preserveHostHeader())
                .uri(notificationServiceUri))             
                .build();
    }
}
