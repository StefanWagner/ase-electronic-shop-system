package at.ac.univie.ase3.apigateway.api.v1.routes;

import at.ac.univie.ase3.apigateway.api.v1.AuthorizationGatewayFilterFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class CartServiceRoutes {
    @Value("${services.uri.carts}")
    private String cartServiceUri;

    @Bean
    public RouteLocator cartRoutes(RouteLocatorBuilder builder
            , AuthorizationGatewayFilterFactory authFilterFactory) {

        return builder.routes()
                .route("getAllCarts", p -> p
                        .path("/v1/carts/carts")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(cartServiceUri))

                .route("addCart", p -> p
                        .path("/v1/carts/cart/add/**")
                        .and().method(HttpMethod.POST)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(cartServiceUri))

                .route("addProductToCart", p -> p
                        .path("/v1/carts/cartitem/add/**")
                        .and().method(HttpMethod.POST)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(cartServiceUri))

                .route("makingCheckout", p -> p
                        .path("/v1/carts/cart/makeCheckout")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(cartServiceUri))
                .route("deleteCartItem", p -> p
                        .path("/v1/carts/cart/delete/**")
                        .and().method(HttpMethod.DELETE)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(cartServiceUri))
                
                .route("deleteAddedCartItem", p -> p
                        .path("/v1/carts/cartitem/delete/**")
                        .and().method(HttpMethod.DELETE)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(cartServiceUri))

                .route("healthcheck", p -> p
                        .path("/v1/carts/actuator/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(cartServiceUri))
                .build();


    }
}
