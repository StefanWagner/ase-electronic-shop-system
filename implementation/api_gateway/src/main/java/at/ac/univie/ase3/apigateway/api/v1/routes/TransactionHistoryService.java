package at.ac.univie.ase3.apigateway.api.v1.routes;

import at.ac.univie.ase3.apigateway.api.v1.AuthorizationGatewayFilterFactory;
import at.ac.univie.ase3.apigateway.api.v1.Roles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class TransactionHistoryService {
    @Value("${services.uri.transactionHistoryService}")
    private String transactionHistoryServiceUri;

    @Bean
    public RouteLocator transactionHistoryServiceRoutes(RouteLocatorBuilder builder
            , AuthorizationGatewayFilterFactory authFilterFactory) {
        return builder.routes()
                .route("getOrders", p -> p
                        .path("/v1/transactionHistoryService/orders/users/{userID}/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(transactionHistoryServiceUri))
                .route("getIncomePerVendor", p -> p
                        .path("/v1/transactionHistoryService/incomes/users/{userID}/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .filter(authFilterFactory.apply(
                                        new AuthorizationGatewayFilterFactory
                                                .Config(Roles.VENDOR)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(transactionHistoryServiceUri))
                .route("healthcheck", p -> p
                        .path("/v1/transactionHistoryService/actuator/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .filter(authFilterFactory.apply(
                                        new AuthorizationGatewayFilterFactory
                                                .Config(Roles.ADMIN)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(transactionHistoryServiceUri))
                .build();
    }
}
