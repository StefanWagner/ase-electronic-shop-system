package at.ac.univie.ase3.apigateway.api.v1.routes;

import at.ac.univie.ase3.apigateway.api.v1.AuthorizationGatewayFilterFactory;
import at.ac.univie.ase3.apigateway.api.v1.Roles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

@Service
public class MarkedProductServiceRoutes {
    @Value("${services.uri.markedp}")
    private String markedpServiceUri;

    @Bean
    public RouteLocator markedProductRoutes(RouteLocatorBuilder builder
            , AuthorizationGatewayFilterFactory authFilterFactory) {

        return builder.routes()
                .route("get_marked_products", p -> p
                        .path("/v1/markedp/marked/users/{userID}/products")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .filter(authFilterFactory.apply(
                                        new AuthorizationGatewayFilterFactory
                                                .Config(Roles.CUSTOMER)))
                                .stripPrefix(2))
                        .uri(markedpServiceUri))
                .route("set_marked_product", p -> p
                        .path("/v1/markedp/marked/users/{userID}/products")
                        .and().method(HttpMethod.POST)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .filter(authFilterFactory.apply(
                                        new AuthorizationGatewayFilterFactory
                                                .Config(Roles.CUSTOMER)))
                                .stripPrefix(2))
                        .uri(markedpServiceUri))
                .route("delete_marked_product", p -> p
                        .path("/v1/markedp/marked/users/{userID}/products/{productID}")
                        .and().method(HttpMethod.DELETE)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .filter(authFilterFactory.apply(
                                        new AuthorizationGatewayFilterFactory
                                                .Config(Roles.CUSTOMER)))
                                .stripPrefix(2))
                        .uri(markedpServiceUri))
                .route("healthcheck", p -> p
                        .path("/v1/markedp/actuator/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .filter(authFilterFactory.apply(
                                        new AuthorizationGatewayFilterFactory
                                                .Config(Roles.ADMIN)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(markedpServiceUri))
                .build();
    }
}
