package at.ac.univie.ase3.apigateway.api.v1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.*;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Map;

@Component
public class AuthorizationGatewayFilterFactory extends AbstractGatewayFilterFactory<AuthorizationGatewayFilterFactory.Config> {
    public AuthorizationGatewayFilterFactory() {
        super(Config.class);
    }
    private static final Logger log = LoggerFactory.getLogger(AuthorizationGatewayFilterFactory.class);
    @Value("${services.uri.auth}")
    private String authBaseUrl;


    public static class Config {
        private Roles necessaryRole;
        public Config(Roles necessaryRole) {
            this.necessaryRole = necessaryRole;
        }
        public Roles getNecessaryRole() {
            return necessaryRole;
        }
    }

    /**
     * checks against the authentication service if the request is authorized
     *
     * @param accessedUserID the userid of the requested resource e.g. userid of the shopping cart which is accessed
     * @param necessaryRole  necessary role for this access
     * @param authToken   given session token from the request header
     * @return true if the request is authorized, false otherwise
     */
    private boolean isAuthorized(int accessedUserID, Roles necessaryRole, String authToken) {
        log.info("Request to authorization server sent, accessed user {}, necessary role {}, token {}"
                , accessedUserID, necessaryRole, authToken);

        RestTemplate restTemplate = new RestTemplate();
        URI uri = URI.create(authBaseUrl+"/authorize?userID="+accessedUserID+"&role="+necessaryRole.name());
        RequestEntity<Void> request = RequestEntity
                .get(uri)
                .header("Authorization", authToken)
                .build();
        try {
            ResponseEntity<String> response = restTemplate.exchange(request, String.class);
            return response.getStatusCode().is2xxSuccessful();
        } catch (Exception e) {
            //restTemplate just throws a exception on a http 403 and doesn't allow me to handle it myself
            log.info(e.getMessage());
        }
        return false;
    }

    private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(httpStatus);
        return response.setComplete();
    }


    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {

            ServerHttpRequest request = exchange.getRequest();
            //TODO remove this return and uncomment below
            //return chain.filter(exchange.mutate().request(request).build());

            HttpHeaders headers = request.getHeaders();
            if (!headers.containsKey("Authorization") || headers.get("Authorization") == null) {
                return onError(exchange, "No Authorization header", HttpStatus.UNAUTHORIZED);
            }

            String sessionToken = headers.get("Authorization").get(0);

            if (!isAuthorized(extractUserID(exchange), config.necessaryRole, sessionToken)) {
                return onError(exchange, "Invalid Authorization header", HttpStatus.FORBIDDEN);
            }

            return chain.filter(exchange.mutate().request(request).build());


        };
    }

    private int extractUserID(ServerWebExchange exchange) {
        Map<String, String> pathVariables = exchange.getAttribute(ServerWebExchangeUtils.URI_TEMPLATE_VARIABLES_ATTRIBUTE);

        if (pathVariables==null || !pathVariables.containsKey("userID")) {
            throw new RuntimeException("userID not found in path");
        }

        return Integer.parseInt(pathVariables.get("userID"));
    }
}
