package at.ac.univie.ase3.apigateway.api.v1.routes;

import at.ac.univie.ase3.apigateway.api.v1.AuthorizationGatewayFilterFactory;
import at.ac.univie.ase3.apigateway.api.v1.Roles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;
import org.springframework.http.HttpMethod;
import javax.annotation.PostConstruct;

@Service
public class InventoryServiceRoutes {
    @Value("${services.uri.inv}")
    private String inventoryServiceUri;

    @Bean
    public RouteLocator inventoryRoutes(RouteLocatorBuilder builder
            , AuthorizationGatewayFilterFactory authFilterFactory) {

        return builder.routes()
                .route("healthcheck", p -> p
                        .path("/v1/inv/healthcheck")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                // .filter(authFilterFactory.apply(
                                //         new AuthorizationGatewayFilterFactory
                                //                 .Config(Roles.CUSTOMER)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(inventoryServiceUri))
                .route("getProducts", p -> p
                        .path("/v1/inv/products")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                // .filter(authFilterFactory.apply(
                                //         new AuthorizationGatewayFilterFactory
                                //                 .Config(Roles.CUSTOMER)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(inventoryServiceUri))
                .route("getProductByID", p -> p
                        .path("/v1/inv/products/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                // .filter(authFilterFactory.apply(
                                //         new AuthorizationGatewayFilterFactory
                                //                 .Config(Roles.CUSTOMER)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(inventoryServiceUri))
                .route("getProductsByVendor", p -> p
                        .path("/v1/inv/products/vendor/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                // .filter(authFilterFactory.apply(
                                //         new AuthorizationGatewayFilterFactory
                                //                 .Config(Roles.CUSTOMER)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(inventoryServiceUri))
                .route("createProduct", p -> p
                        .path("/v1/inv/products")
                        .and().method(HttpMethod.POST)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                // .filter(authFilterFactory.apply(
                                //         new AuthorizationGatewayFilterFactory
                                //                 .Config(Roles.VENDOR)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(inventoryServiceUri))
                .route("updateProduct", p -> p
                        .path("/v1/inv/products/**")
                        .and().method(HttpMethod.PATCH)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                // .filter(authFilterFactory.apply(
                                //         new AuthorizationGatewayFilterFactory
                                //                 .Config(Roles.VENDOR)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(inventoryServiceUri))
                .route("deleteProduct", p -> p
                        .path("/v1/inv/products/**")
                        .and().method(HttpMethod.DELETE)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                // .filter(authFilterFactory.apply(
                                //         new AuthorizationGatewayFilterFactory
                                //                 .Config(Roles.VENDOR)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(inventoryServiceUri))
                .build();
    }
}
