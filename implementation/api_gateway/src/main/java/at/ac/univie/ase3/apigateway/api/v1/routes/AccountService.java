package at.ac.univie.ase3.apigateway.api.v1.routes;

import at.ac.univie.ase3.apigateway.api.v1.AuthorizationGatewayFilterFactory;
import at.ac.univie.ase3.apigateway.api.v1.Roles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

@Service
public class AccountService {
    @Value("${services.uri.accountService}")
    private String accountServiceUri;

    @Bean
    public RouteLocator accountRoutes(RouteLocatorBuilder builder
            , AuthorizationGatewayFilterFactory authFilterFactory) {
        return builder.routes()
                .route("getProfit", p -> p
                        .path("/v1/accountService/profits/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                // .filter(authFilterFactory.apply(
                                //         new AuthorizationGatewayFilterFactory
                                //                 .Config(Roles.VENDOR)))
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(accountServiceUri))

                .route("health", p -> p
                        .path("/v1/accountService/health")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(accountServiceUri))

                .build();
    }
}
