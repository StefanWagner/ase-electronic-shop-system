package at.ac.univie.ase3.apigateway.api.v1.routes;

import at.ac.univie.ase3.apigateway.api.v1.AuthorizationGatewayFilterFactory;
import at.ac.univie.ase3.apigateway.api.v1.Roles;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

import javax.annotation.PostConstruct;

@Service
public class CheckoutServiceRoutes {
    @Value("${services.uri.checkos}")
    private String checkoutServiceUri;

    @Bean
    public RouteLocator checkoutRoutes(RouteLocatorBuilder builder
            , AuthorizationGatewayFilterFactory authFilterFactory) {

        return builder.routes()
                .route("getAll", p -> p
                        .path("/v1/checkos/getOrders")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(checkoutServiceUri))

                .route("getOrderById", p -> p
                        .path("/v1/checkos/order/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(checkoutServiceUri))


                .route("placeOrder", p -> p
                        .path("/v1/checkos/placeOrder")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(checkoutServiceUri))

                .route("addOrder", p -> p
                        .path("/v1/checkos/placeOrder/add")
                        .and().method(HttpMethod.POST)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(checkoutServiceUri))
                
                .route("deleteOrder", p -> p
                        .path("/v1/checkos/order/delete/**")
                        .and().method(HttpMethod.DELETE)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(checkoutServiceUri))

                .route("healthcheck", p -> p
                        .path("/v1/checkos/actuator/**")
                        .and().method(HttpMethod.GET)
                        .filters(gatewayFilterSpec -> gatewayFilterSpec
                                .stripPrefix(2)
                                .preserveHostHeader())
                        .uri(checkoutServiceUri))

                .build();






    }
}
