db.createUser(
    {
        user: "user",
        pwd: "password",
        roles: [
            {
                role: "readWrite",
                db: "transaction_history_service_database"
            }
        ]
    }
);

db.orders.createIndex({ "userID": 1, "orderID": 1}, { unique: true});