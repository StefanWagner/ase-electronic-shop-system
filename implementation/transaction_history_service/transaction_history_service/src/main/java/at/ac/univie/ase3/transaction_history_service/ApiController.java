package at.ac.univie.ase3.transaction_history_service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.univie.ase3.transaction_history_service.entities.Order;

@RestController
public class ApiController {
  private static final Logger log = LoggerFactory.getLogger(ApiController.class);

  @Autowired
  private OrderRepository orderRepository;

  /**
   * This method provides access to the orders of the customer.
   * 
   * @param customerID The customer that orders are requested.
   * @param orderID    The optional orderID, if only a specific order is
   *                   requiered.
   * @return The list of orders for the given customer.
   */
  @GetMapping("/orders/users/{userID}")
  public List<Order> getOrders(@PathVariable(value = "userID") int customerID,
      @RequestParam Optional<Integer> orderID) {
    log.info("Order request with userID: {}.", customerID);
    if (orderID.isPresent()) {
      log.info("Specific order request with orderID: {}.", orderID.get());
      return orderRepository.findOrderByCustomerIDAndOrderID(customerID, orderID.get());
    } else {
      return orderRepository.findOrdersByCustomerID(customerID);
    }
  }

  /**
   * This method provides access to the income in the given time period.
   * 
   * @param vendorID The vendor that income is requested.
   * @param from     The start timestamp of the time period.
   * @param to       The end timestamp of the time period.
   * @return The income for the given vendor in a specific time period.
   */
  @GetMapping("/incomes/users/{userID}")
  public ResponseEntity<String> getIncome(@PathVariable(value = "userID") int vendorID, @RequestParam Instant from,
      @RequestParam Instant to) {
    log.info("Income request with userID: {}, from {} and to {}.", vendorID, from, to);
    Double sum = 0.0;
    for (Order order : orderRepository.findOrdersByVendorID(vendorID)) {
      if (order.getTimestamp().isAfter(from) && order.getTimestamp().isBefore(to)) {
        sum += order.sumPriceByVendorID(vendorID);
      }
    }
    return ResponseEntity.ok("{\"income\": " + sum + "}");
  }

}