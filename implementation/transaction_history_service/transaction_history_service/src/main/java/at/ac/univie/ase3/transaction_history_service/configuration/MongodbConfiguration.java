package at.ac.univie.ase3.transaction_history_service.configuration;

import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoClientFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Configuration
public class MongodbConfiguration {
  private static final Logger log = LoggerFactory.getLogger(MongodbConfiguration.class);

  @Value("${mongo.host}")
  private String mongoHost;

  @Value("${mongo.port}")
  private int mongoPort;

  @Value("${mongo.username}")
  private String mongoUsername;

  @Value("${mongo.password}")
  private String mongoPassword;

  @Value("${mongo.database}")
  private String mongoDatabase;

  public @Bean MongoClientFactoryBean mongo() {
    MongoClientFactoryBean mongo = new MongoClientFactoryBean();
    mongo.setHost(mongoHost);
    mongo.setPort(mongoPort);
    mongo.setCredential(new MongoCredential[] {
        MongoCredential.createCredential(mongoUsername, mongoDatabase, mongoPassword.toCharArray()) });
    log.info("MongoClientFactoryBean created.");
    return mongo;
  }

  @Bean
  public MongoDatabaseFactory mongoDatabaseFactory(MongoClient client) {
    log.info("MongoDatabaseFactory created.");
    return new SimpleMongoClientDatabaseFactory(client, mongoDatabase);
  }

  @Bean
  public MongoTemplate mongoTemplate(MongoDatabaseFactory databaseFactory) {
    log.info("MongoTemplate created.");
    return new MongoTemplate(databaseFactory);
  }
}
