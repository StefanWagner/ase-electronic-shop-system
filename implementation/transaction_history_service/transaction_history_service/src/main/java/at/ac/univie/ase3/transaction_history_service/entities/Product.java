package at.ac.univie.ase3.transaction_history_service.entities;

public class Product {
  private Integer vendorID;
  private Integer productID;
  private Integer amount;
  private Double price;

  public Product() {
  }

  public Product(Integer vendorID, Integer productID, Integer amount, Double price) {
    this.vendorID = vendorID;
    this.productID = productID;
    this.amount = amount;
    this.price = price;
  }

  public Integer getVendorID() {
    return vendorID;
  }

  public void setVendorID(Integer vendorID) {
    this.vendorID = vendorID;
  }

  public Integer getProductID() {
    return productID;
  }

  public void setProductID(Integer productID) {
    this.productID = productID;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String toString() {
    String vendorID = "vendorID: " + this.vendorID;
    String productID = "productID: " + this.productID;
    String amount = "amount: " + this.amount;
    String price = "price: " + this.price;
    return vendorID + ", " + productID + ", " + amount + ", " + price;
  }
}