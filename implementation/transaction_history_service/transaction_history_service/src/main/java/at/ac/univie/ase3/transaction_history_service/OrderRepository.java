package at.ac.univie.ase3.transaction_history_service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import org.springframework.stereotype.Repository;

import at.ac.univie.ase3.transaction_history_service.entities.Order;

@Repository
public class OrderRepository {
  @Autowired
  private MongoTemplate mongoTemplate;

  public List<Order> findOrdersByCustomerID(int customerID) {
    return mongoTemplate.find(Query.query(where("customerID").is(customerID)), Order.class);
  }

  public List<Order> findOrderByCustomerIDAndOrderID(int customerID, int orderID) {
    return mongoTemplate.find(Query.query(where("customerID").is(customerID).and("orderID").is(orderID)), Order.class);
  }

  public List<Order> findOrdersByVendorID(int vendorID) {
    return mongoTemplate.find(Query.query(where("products").elemMatch(where("vendorID").is(vendorID))), Order.class);
  }

  public List<Order> findAll() {
    return mongoTemplate.findAll(Order.class);
  }

  public void insertOrder(Order order) {
    mongoTemplate.insert(order);
  }

}
