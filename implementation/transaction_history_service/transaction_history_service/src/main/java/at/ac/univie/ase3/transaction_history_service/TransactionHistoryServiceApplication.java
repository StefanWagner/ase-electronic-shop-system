package at.ac.univie.ase3.transaction_history_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionHistoryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionHistoryServiceApplication.class, args);
	}

}
