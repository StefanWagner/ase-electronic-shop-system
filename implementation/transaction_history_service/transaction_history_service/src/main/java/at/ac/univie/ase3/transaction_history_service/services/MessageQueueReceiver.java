package at.ac.univie.ase3.transaction_history_service.services;

import org.springframework.stereotype.Service;

import at.ac.univie.ase3.transaction_history_service.OrderRepository;
import at.ac.univie.ase3.transaction_history_service.entities.Order;
import at.ac.univie.ase3.transaction_history_service.entities.Product;

import java.time.Instant;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

@Service
public class MessageQueueReceiver {
  private static final Logger log = LoggerFactory.getLogger(MessageQueueReceiver.class);
  @Autowired
  private OrderRepository orderRepository;

  /**
   * This method processes the incoming messages from the kafka message broker.
   * The subscribed topic is "orders". Incoming orders will be stored in the
   * database.
   * 
   * @param key  The key of the message.
   * @param data The data of the message.
   */
  @KafkaListener(id = "transaction_history_service_listener", topics = "orders")
  public void processMessage(String key, String data) {
    try {
      log.info("Received message with key: {} value: {}", key, data);
      ObjectMapper objectMapper = new ObjectMapper();
      JsonNode jsonData = objectMapper.readTree(data);
      JsonNode jsonProducts = jsonData.get("products");
      Product[] productsArray = objectMapper.readValue(jsonProducts.toString(), Product[].class);
      ArrayList<Product> products = new ArrayList<Product>();
      for (Product product : productsArray) {
        products.add(product);
      }
      Order order = new Order(jsonData.get("orderID").asInt(), jsonData.get("customerID").asInt(),
          Instant.parse(jsonData.get("timestamp").asText()), products);
      orderRepository.insertOrder(order);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
  }
}
