package at.ac.univie.ase3.transaction_history_service.entities;

import org.springframework.data.annotation.Id;

import java.time.Instant;
import java.util.ArrayList;

public class Order {
  @Id
  private Integer orderID;
  private Integer customerID;
  private Instant timestamp;
  private ArrayList<Product> products;

  public Order(Integer orderID, Integer customerID, Instant timestamp, ArrayList<Product> products) {
    this.orderID = orderID;
    this.customerID = customerID;
    this.timestamp = timestamp;
    this.products = products;
  }

  public Integer getOrderID() {
    return orderID;
  }

  public void setOrderID(Integer orderID) {
    this.orderID = orderID;
  }

  public Integer getCustomerID() {
    return customerID;
  }

  public void setCustomerID(Integer customerID) {
    this.customerID = customerID;
  }

  public Instant getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Instant timestamp) {
    this.timestamp = timestamp;
  }

  public ArrayList<Product> getProducts() {
    return products;
  }

  public void setProducts(ArrayList<Product> products) {
    this.products = products;
  }

  /**
   * This method calculates the income of the order for a given vendor.
   * 
   * @param vendorID The vendor that income should be calculated.
   * @return The vendor specific income.
   */
  public Double sumPriceByVendorID(Integer vendorID) {
    Double sum = 0.0;
    for (Product product : this.products) {
      if (product.getVendorID().equals(vendorID)) {
        sum += product.getPrice() * product.getAmount();
      }
    }
    return sum;
  }
}
