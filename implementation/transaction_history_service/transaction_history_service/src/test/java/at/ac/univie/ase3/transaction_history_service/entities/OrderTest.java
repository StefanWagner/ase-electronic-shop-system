package at.ac.univie.ase3.transaction_history_service.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class OrderTest {

  @Test
  void sumPriceByVendorID_shouldReturnTheCorrectIncome() {
    Product product1 = new Product(1, 1, 1, 10.0);
    Product product2 = new Product(1, 2, 2, 20.0);
    ArrayList<Product> products1 = new ArrayList<Product>();
    products1.add(product1);
    products1.add(product2);
    Order order1 = new Order(1, 1, Instant.parse("1971-01-01T00:00:00.000Z"), products1);

    Product product3 = new Product(1, 3, 3, 30.0);
    Product product4 = new Product(2, 4, 4, 40.0);
    ArrayList<Product> products2 = new ArrayList<Product>();
    products2.add(product3);
    products2.add(product4);
    Order order2 = new Order(2, 2, Instant.parse("1972-01-01T00:00:00.000Z"), products2);

    Product product5 = new Product(2, 5, 5, 50.0);
    Product product6 = new Product(2, 6, 6, 60.0);
    ArrayList<Product> products3 = new ArrayList<Product>();
    products3.add(product5);
    products3.add(product6);
    Order order3 = new Order(3, 3, Instant.parse("1973-01-01T00:00:00.000Z"), products3);

    assertEquals(50, order1.sumPriceByVendorID(1));
    assertEquals(90, order2.sumPriceByVendorID(1));
    assertEquals(0, order3.sumPriceByVendorID(1));

    assertEquals(0, order1.sumPriceByVendorID(2));
    assertEquals(160, order2.sumPriceByVendorID(2));
    assertEquals(610, order3.sumPriceByVendorID(2));
  }
}
