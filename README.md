# ASE Electronic Shop System

## Description

The task was to design and implement an electronic shop system that fulfills the following requirements:

* The system should provide an API Gateway, as well as integration or end-to-end tests, and a command line client for testing.

* The system, i.e., all services, must support event-driven communication, e.g., via the publish/subscribe pattern.

* The system must support a product inventory service: vendors manage products in their inventory.

* The system must support an account service: vendors can track their profit from sold and bought products.

* The system must support a transaction history service: vendors and customers can see the history of their products in the eShop system.

* The system must support a cart service: customers add products to their carts.

* The system must support a checkout service: customers buy the products in their cart.

* The system must support a price adjustment service to make price recommendations for products.

* The system must support a marked products service.

* The system must support a shipment service: customers put in their shipment information and the system ships the product by updating the shipment status, e.g., ready-for-shipment and out-for-delivery.

* The system must support an email notification service: eShop informs customers about their shipment delivery, e.g., ready-for-shipment and out-for-delivery notifications. Also, eShop informs customers about a price adjustment of a marked product.

* The system must support a rating service: customers rate the products.

## Team

5 students

## Technology

Java 11, Spring Boot Web, Spring for Apache Kafka, Spring for MongoDB, Spring Actuator, Spring Cloud Gateway, Logback, JUnit 5, MongoDB, Python, Flask, Beautiful Soup 4, Logging, Unittest, Gradle, Nginx, Bootstrap, JavaScript, HTML, CSS

## My Responsibility

I was responsible for the price adjustment service and the transaction history service. I also contributed at the API gateway and the frontend service.

## Difficulties faced and lessons learned

To come up with a solution and to make sure that everyone has the same understanding was sometimes really hard. The new learned domain driven design and the point that the whole design process was done in online meetings because of covid-19 restrictions were reasons for the intensive design phase. But at the end of the design phase we had worked out a good solution. Unfortunately not all team members were able to implement what we have planned, so I decided to upload here only the source code of the microservices that were implemented by me.